var socket = new WebSocket("ws://" + localStorage['ipaddr']);

request = {
    "name": "authenticate",
    "type": "request",
    "id": localStorage['devid'],
    "device_id": localStorage['devid'],
    "options": {
        "password": ""
    }
}

var queue = 0;

// FUNCTIONS

function sec2time(timeInSeconds) {
    var pad = function(num, size) { return ('000' + num).slice(size * -1); },
    time = parseFloat(timeInSeconds).toFixed(3),
    hours = Math.floor(time / 60 / 60),
    minutes = Math.floor(time / 60) % 60,
    seconds = Math.floor(time - minutes * 60),
    milliseconds = time.slice(-3);

    return pad(hours, 2) + ':' + pad(minutes, 2) + ':' + pad(seconds, 2);
}

function showBar(text) {
    document.getElementById('snackbar').innerHTML = text;   
    var x = document.getElementById("snackbar")
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}

function sendInfo() {
    request = {
    "name": "get_playback_overview",
    "type": "request",
    "id": localStorage['devid'],
    "device_id": localStorage['devid'],
    "options": { }
    }
    
    socket.send(JSON.stringify(request));
}

function saveSettings() {
    localStorage.setItem('ipaddr', document.getElementById("ip").value);
    localStorage.setItem('devid', document.getElementById("devid").value);
    showBar("Settings sucessfully saved, reload page to apply changes!");
}

function setPlaybackState(state) {
    request = {
    "name": state,
    "type": "request",
    "id": localStorage['devid'],
    "device_id": localStorage['devid'],
    "options": { }
    }
    
    socket.send(JSON.stringify(request));
}


function seekTo(seconds) {
    console.log(seconds);
    request = {
        "name": "seek_to",
        "type": "request",
        "id": localStorage['devid'],
        "device_id": localStorage['devid'],
        "options": {
            "position": parseInt(seconds)
        }
    }
    socket.send(JSON.stringify(request));
}


function playTrackInQueue(id) {
    request = {
        "name": "play_at_index",
        "type": "request",
        "id": localStorage['devid'],
        "device_id": localStorage['devid'],
        "options": {
            "index": id,
        }
    }
    console.log(request);
    socket.send(JSON.stringify(request));
}


function setupVolume(vol) {
    request = {
        "name": "set_volume",
        "type": "request",
        "id": localStorage['devid'],
        "device_id": localStorage['devid'],
        "options": { 
            "volume": vol / 100
        }
    }
    console.log(request);
    socket.send(JSON.stringify(request));
}

function requestPlaylist() {
    request_queue = {
    "name": "query_play_queue_tracks",
    "type": "request",
    "id": localStorage['devid'],
    "device_id": localStorage['devid'],
    "options": {
        "count_only": false, /* optional; default false */
        "type": "live", /* optional; default "live" */
        }
    }
    socket.send(JSON.stringify(request_queue));
}


// SOCKETS

socket.onopen = function() {
  socket.send(JSON.stringify(request));  
  showBar("Сonnected logging in...");
  document.getElementById('connection').style.backgroundImage = "url('icons/connect.png')";
};

socket.onclose = function(event) {
  console.log("Disconnected! Code: " + event.code + " Reason:" + event.reason);
  document.getElementById('connection').style.backgroundImage = "url('icons/disconnect.png')";
  if (event.reason == "unauthenticated")
  {
     showBar("Invalid password, disconnected! <a href='google.ru'>Options</a>"); 
  } else {
      showBar("Disconnected! <input type='button' value='Options' onclick='document.getElementById(\"settings\").style.visibility = \"visible\";'>");
  }
};

socket.onmessage = function(event) {
  //console.log("Получены данные " + event.data);
  cmd = JSON.parse(event.data);
  if (cmd['name'] == "authenticate") {
    showBar("Successfully connected!");
    request = {
    "name": "get_playback_overview",
    "type": "request",
    "id": localStorage['devid'],
    "device_id": localStorage['devid'],
    "options": { }
    }
    requestPlaylist();
 }
 console.log(cmd['name']);  
 if (cmd['name'] == "playback_overview_changed") {
   requestPlaylist();
 }
  
  if (cmd['name'] == "playback_overview_changed" || cmd['name'] == "get_playback_overview") {
    document.getElementById('track-name').innerHTML = cmd['options']['playing_track']['artist'] + " - " + cmd['options']['playing_track']['title'];
    //console.log(typeof(cmd['options']['playing_current_time']));
    document.getElementById('track-dur').innerHTML = sec2time(cmd['options']['playing_current_time']);
    document.getElementById('volume-text').innerHTML = "Volume: " + cmd['options']['volume'] * 100 + "%";
    vol.value = Math.floor(cmd['options']['volume'] * 100);
    myRange.max = Math.floor(cmd['options']['playing_duration']);
    myRange.value = Math.floor(cmd['options']['playing_current_time']);
    queue = cmd['options']['play_queue_position'];
    setTimeout(sendInfo, 1000);
    if (cmd['options']['state'] == "paused") {
        document.getElementById('play').style.backgroundImage = "url('icons/play.png')";
    } else {
        document.getElementById('play').style.backgroundImage = "url('icons/pause.png')";
    }
  }
  
  if (cmd['name'] == "query_play_queue_tracks") {
     document.getElementById('blockplaylist').innerHTML  = "";
     console.log("selected", queue);
    for (index = 0; index < cmd['options']['data'].length; index++) {
        // bad code is here, please help if you know less hacky way to do same thing...
        if (queue == index) {
            document.getElementById('blockplaylist').innerHTML = document.getElementById('blockplaylist').innerHTML + "<input type='button' class='playlistbtn-selected' value='" + cmd['options']['data'][index]['artist'] + " - " + cmd['options']['data'][index]['title'] + "\n" + cmd['options']['data'][index]['album'] + "' onclick='playTrackInQueue(" + index + ");'>";
        } else {
            document.getElementById('blockplaylist').innerHTML = document.getElementById('blockplaylist').innerHTML + "<input type='button' class='playlistbtn' value='" + cmd['options']['data'][index]['artist'] + " - " + cmd['options']['data'][index]['title'] + "\n" + cmd['options']['data'][index]['album'] + "' onclick='playTrackInQueue(" + index + ");'>";            
        }
    }
  }
  
  if (cmd['name'] == 'invalid') {
    showBar("Invalid request!");
  }
};

socket.onerror = function(error) {
  showBar("Error:" + error.message);
};