# musikqb
a web-interface for [musikcube](https://github.com/clangen/musikcube) player. using **websocket** and pure html/css/js

# installation

### easiest way
* clone this repository
* open ``index.html`` file in your browser
* setup ip/device id in "options" window
* enjoy the music control!
### harder, but not so
* clone this repository
* using webserver copy all files from repository in ``htdocs`` or ``public_html`` folder
* open your browser and go to your website adress
* setup ip/device id in "options" window
* enjoy the music control!